/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Text,
  Switch,
  Button,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isAdmin: false,
      warning: '',
    };
  }

  loginClickHandler() {
    if (this.state.isAdmin) {
      if (this.state.email !== 'admin@gmail.com') {
        this.setState({warning: 'enter valid email'});
      } else if (this.state.password !== '12345678') {
        this.setState({warning: 'enter valid password'});
      } else {
        this.setState({warning: ''});
        this.props.navigation.navigate('EventList', {
          isAdmin: this.state.isAdmin,
        });
      }
    } else {
      if (this.state.email !== 'husen@gmail.com') {
        this.setState({warning: 'enter valid email'});
      } else if (this.state.password !== '12345678') {
        this.setState({warning: 'enter valid password'});
      } else {
        this.setState({warning: ''});
        this.props.navigation.navigate('EventList', {
          isAdmin: this.state.isAdmin,
        });
      }
    }
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{color: '#2776fa', fontSize: 20, marginBottom: 30}}>
          LOGIN
        </Text>
        <Text>{this.state.warning}</Text>
        <TextInput
          placeholder="Email"
          style={styles.inputView}
          onChangeText={text => this.setState({email: text})}
          placeholderTextSize={15}
          fontSize={17}
        />
        <TextInput
          returnKeyType="done"
          style={styles.inputView}
          placeholder="password"
          onChangeText={text => this.setState({password: text})}
          placeholderTextSize={15}
          fontSize={17}
        />

        <View style={{flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
          <Switch
            value={this.state.isAdmin}
            onValueChange={() => this.setState({isAdmin: !this.state.isAdmin})}
          />
          <Text>Is Admin ?</Text>
        </View>

        <Button
          title="Login"
          style={{width: 100}}
          color="#2776fa"
          onPress={() => this.loginClickHandler()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputView: {
    color: '#000',
    width: 300,
    fontSize: 17,
    padding: 10,
    marginBottom: 10,
    borderColor: '#D6D6D6',
    borderWidth: 1,
    borderRadius: 5,
    textAlign: 'center',
  },
});

export default Login;
