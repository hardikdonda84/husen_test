/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Text,
  Switch,
  Button,
  StatusBar,
  AsyncStorage,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppNavigator from './app_navigator';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      warning: '',
    };
    this.data = [
      {
        name: 'event 1',
        description: 'description 1',
        place: 'place 1',
        startTime: '10-01-2020 08:00 AM',
        endTime: '10-01-2020 10:00 AM',
        people_count: 10,
      },
      {
        name: 'event 2',
        description: 'description 2',
        place: 'place 2',
        startTime: '10-01-2020 08:00 AM',
        endTime: '10-01-2020 10:00 AM',
        people_count: 20,
      },
    ];
  }

  componentWillMount() {
    this._retrieveData();
  }
  _retrieveData = async () => {
    try {
      let value = await AsyncStorage.getItem('master');
      if (value == null) {
        console.warn('null');
        AsyncStorage.setItem('master', JSON.stringify(this.data));
      } else {
        console.warn('already');
        console.warn(value);
      }
    } catch (error) {
      console.warn('error');
      AsyncStorage.setItem('master', JSON.stringify(this.data));
    }
  };

  render() {
    return <AppNavigator />;
  }
}

const styles = StyleSheet.create({
  inputView: {
    color: '#000',
    width: 300,
    fontSize: 17,
    padding: 10,
    marginBottom: 10,
    borderColor: '#D6D6D6',
    borderWidth: 1,
    borderRadius: 5,
    textAlign: 'center',
  },
});

export default App;
