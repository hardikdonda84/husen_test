import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Login from './login';
import EventList from './event_list';
import EventDetail from './event_detail';

const CNavigate = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null,
      },
    },
    EventList: {
      screen: EventList,
      navigationOptions: {
        header: null,
      },
    },
    EventDetail: {
      screen: EventDetail,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Login',
  },
);

const AppNavigator = createAppContainer(CNavigate);

export default AppNavigator;
