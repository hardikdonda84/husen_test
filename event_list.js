/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Text,
  Switch,
  Button,
  StatusBar,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppNavigator from './app_navigator';

class EventList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isAdmin: false,
    };
  }

  componentWillMount() {
    const {navigation} = this.props;
    const isAdmin = navigation.getParam('isAdmin');
    this.setState({isAdmin: isAdmin});
    this._retrieveData();
  }

  _retrieveData = async () => {
    try {
      let value = await AsyncStorage.getItem('master');
      if (value !== null) {
        this.setState({data: JSON.parse(value)});
      }
    } catch (error) {}
  };

  rowClick(item, index) {
    console.warn(this.state.isAdmin);
    if (this.state.isAdmin) {
      this.props.navigation.navigate('EventDetail', {
        item: item,
        index: index,
      });
    }
  }
  render() {
    return (
      <FlatList
        bounces={false}
        keyExtractor={({id}, index) => id}
        data={this.state.data}
        renderItem={({item, index}) => (
          <TouchableOpacity onPress={() => this.rowClick(item, index)}>
            <View style={styles.container}>
              <Text>{item.name}</Text>
              <Text>{item.description}</Text>
              <Text>{item.place}</Text>
              <Text>{item.people_count}</Text>
              <Text>{item.startTime}</Text>
              <Text>{item.endTime}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  inputView: {
    color: '#000',
    width: 300,
    fontSize: 17,
    padding: 10,
    marginBottom: 10,
    borderColor: '#D6D6D6',
    borderWidth: 1,
    borderRadius: 5,
    textAlign: 'center',
  },
  container: {
    backgroundColor: '#fff',
    borderRadius: 3,
    borderLeftWidth: 4,
    borderLeftColor: '#2776fa',
    elevation: 3,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 10,
    marginBottom: 10,
    shadowOffset: {width: 0.5, height: 0.5},
    shadowColor: 'black',
    shadowOpacity: 0.4,
    padding: 12,
  },
});

export default EventList;
