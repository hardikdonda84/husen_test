/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Text,
  Switch,
  Button,
  StatusBar,
  FlatList,
  AsyncStorage,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppNavigator from './app_navigator';
import {TouchableOpacity} from 'react-native-gesture-handler';

class EventDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      selectedItem: {},
      selectedItemPosition: 0,
      event_name: '',
      event_people_count: 0,
      event_description: '',
      event_place: '',
      event_start_time: '',
      event_end_time: '',
    };
  }

  componentWillMount() {
    const {navigation} = this.props;
    const item = navigation.getParam('item');
    const index = navigation.getParam('index');
    this.setState({
      event_name: item.name,
      event_people_count: item.people_count,
      event_description: item.description,
      event_place: item.place,
      event_end_time: item.endTime,
      event_start_time: item.startTime,
    });
    this.setState({selectedItem: item});
    this.setState({index: index});
    this._retrieveData();
  }

  _retrieveData = async () => {
    try {
      let value = await AsyncStorage.getItem('master');
      if (value !== null) {
        console.warn(value);
        this.setState({data: JSON.parse(value)});
      }
    } catch (error) {}
  };

  updateEventHandler() {
    let newArray = [...this.state.data];
    var obj = {
      name: this.state.event_name,
      description: this.state.event_description,
      place: this.state.event_place,
      startTime: this.state.event_start_time,
      endTime: this.state.event_end_time,
      people_count: this.state.event_people_count,
    };
    newArray[this.state.index] = obj;
    console.warn(newArray);
    this.setState({data: newArray}, function() {
      AsyncStorage.setItem('master', JSON.stringify(this.state.data));
      console.warn('updated successfully.');
    });
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 20, marginBottom: 20}}>Update Event</Text>
        <TextInput
          placeholder="Event Name"
          style={styles.inputView}
          value={this.state.event_name}
          onChangeText={text => this.setState({event_name: text})}
          placeholderTextSize={15}
          fontSize={17}
        />
        <TextInput
          placeholder="Event Place"
          style={styles.inputView}
          value={this.state.event_place}
          onChangeText={text => this.setState({event_place: text})}
          placeholderTextSize={15}
          fontSize={17}
        />
        <TextInput
          placeholder="Event description"
          style={styles.inputView}
          value={this.state.event_description}
          onChangeText={text => this.setState({event_description: text})}
          placeholderTextSize={15}
          fontSize={17}
        />
        <TextInput
          placeholder="Event people Count"
          style={styles.inputView}
          value={this.state.event_people_count}
          onChangeText={text => this.setState({event_people_count: text})}
          placeholderTextSize={15}
          fontSize={17}
        />

        <TextInput
          placeholder="Event Start Time"
          style={styles.inputView}
          value={this.state.event_start_time}
          onChangeText={text => this.setState({event_start_time: text})}
          placeholderTextSize={15}
          fontSize={17}
        />
        <TextInput
          placeholder="Event End Time"
          style={styles.inputView}
          value={this.state.event_end_time}
          onChangeText={text => this.setState({event_end_time: text})}
          placeholderTextSize={15}
          fontSize={17}
        />

        <Button
          title="Update Event"
          color="#2776fa"
          onPress={() => this.updateEventHandler()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputView: {
    color: '#000',
    width: 300,
    fontSize: 17,
    padding: 10,
    marginBottom: 10,
    borderColor: '#D6D6D6',
    borderWidth: 1,
    borderRadius: 5,
    textAlign: 'center',
  },
  container: {
    backgroundColor: '#fff',
    borderRadius: 3,
    borderLeftWidth: 4,
    borderLeftColor: '#2776fa',
    elevation: 3,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 10,
    marginBottom: 10,
    shadowOffset: {width: 0.5, height: 0.5},
    shadowColor: 'black',
    shadowOpacity: 0.4,
    padding: 12,
  },
});

export default EventDetail;
